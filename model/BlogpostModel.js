const mongoose = require("mongoose");

const blogPost_schema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    main: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    created_at: {
        type: String,
        required: true
    }  
})


module.exports = mongoose.model("blogpost", blogPost_schema)
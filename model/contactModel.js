const mongoose = require("mongoose");

const contant_schema = mongoose.Schema({  
    fullName: {
        type: String
      },
      phoneNumber: {
        type: String
      },
      email: {
        type: String
      },
      message: {
        type: String
      },
      address: {
        type: String
      },
      date: {
        type: String
      },
})


module.exports = mongoose.model("Contant", contant_schema)
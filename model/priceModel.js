const mongoose = require('mongoose')

const Price_Schema = new mongoose.Schema({
  SubCategoryID: {
    type: String,
    required: ' Category ID is Required'
  },

  SubProductname: {
    type: String,
    required: 'SubProductname is Required'
  },

  PriceDes: {
    type: String,
    required: 'PriceDes is Required'
  },

  Price: {
    type: String,
    required: 'Price is Required'
  },

})

module.exports = mongoose.model('Pricelist', Price_Schema)

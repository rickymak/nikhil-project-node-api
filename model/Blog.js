const mongoose = require('mongoose')

const blogSchema = new mongoose.Schema({
  MaincategoryID: {
    type: String,
    required: 'Title is Required'
  },
  Subcategory: {
    type: String,
    required: 'Title is Required'
  },
  imageUrl: {
    type: String
  },
  MainCategory: {
    type: String
  }
})

module.exports = mongoose.model('Blog', blogSchema)

const mongoose = require("mongoose");

const post_schema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
   
    Image: {
        type: String,       
    },
    type: {
        type: String,       
    },
    UserType:{
        type: String, 
        required: true  
    },
    gender:{
        type: String, 
    },
    age:{
        type: String, 
    },
    Category: {
        type: String,
        
    },
    CategoryId: {
        type: String,
        
    },
    SubCategory: {
        type: String,
        
    },
    SubCategoryId: {
        type: String,
        
    },
    open: {
        type: String,
        
    }
})


module.exports = mongoose.model("registration", post_schema)
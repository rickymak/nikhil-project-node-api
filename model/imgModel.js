const mongoose = require('mongoose')

const ImgSchema = new mongoose.Schema({

  imageUrl: {
    type: String
  }
 
})

module.exports = mongoose.model('UploadImg', ImgSchema)
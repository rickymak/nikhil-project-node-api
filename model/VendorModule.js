const mongoose = require("mongoose");

const post_schema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    ImageUrl: {
        type: String,
        
    },
    Category: {
        type: String,
        
    },
    CategoryId: {
        type: String,
        
    },
    SubCategory: {
        type: String,
        
    },
    SubCategoryId: {
        type: String,
        
    }
})


module.exports = mongoose.model("Vendor", post_schema)
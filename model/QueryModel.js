const mongoose = require("mongoose");

const query_schema = mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    MainCategoryId: {
        type: String,
        
    },
    SubCategoryId: {
        type: String,
        
    },
    Date: {
        type: String,
        
    },
    AllottedTo: {
        type: String,
        
    },
    AllottedId: {
        type: String,
        
    },
    AllottedDate: {
        type: String,
        
    },
    BillNo: {
        type: String,
        
    },
    BillDetails: {
        type: String,
        
    },
    BillPrice: {
        type: String,
        
    },
    BillDate: {
        type: String,
        
    },
    Status: {
        type: String,
        
    },
    Message:{
        type: String,
    },
    ServiceTime:{
        type: String,
    },
    
})


module.exports = mongoose.model("Query", query_schema)
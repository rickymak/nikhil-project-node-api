const mongoose = require('mongoose')

const fontSchema = new mongoose.Schema({
  name: {
    type: String,
    required: 'Title is Required'
  },
  size: {
    type: String,
    required: 'Title is Required'
  }

})

module.exports = mongoose.model('font', fontSchema)

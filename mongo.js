const mongoose = require("mongoose");
require("dotenv").config();
const mongoDbErrors = require('mongoose-mongodb-errors')
mongoose.Promise = global.Promise;
mongoose.plugin(mongoDbErrors);
mongoose.connect(process.env.MONGOURI);

   
const express = require("express");
require('express-async-errors');
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser")
const morgan = require("morgan") ; 
//database connection
require("./mongo")
//Models
require("./model/Post");
require("./model/registration");
require("./model/BlogpostModel");
require('./model/Blog')
require("./model/Maincateory");
require("./model/priceModel");
require("./model/VendorModule");
require("./model/QueryModel");
require("./model/imgModel");
require("./model/SubCategoryListModel");
require("./model/contactModel");
require("./model/fontModel");  

 app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
 app.use(bodyParser.json({ type: 'application/*+json' }))
 .use(morgan());


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept-Language, Authorization");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  if ('OPTIONS' === req.method) {
      res.sendStatus(200);
  } else {
      next();
  }
});


//Routes
app.use("/posts", require("./routes/posts"))
app.use("/reg", require("./routes/registration"))
app.use("/blog", require("./routes/blogPostRoute"))
app.use("/img", require("./routes/imgUpload"))
app.use("/Main", require("./routes/cateroryMain"))
app.use("/Price", require("./routes/priceRoute"))
app.use("/Vendor", require("./routes/VendorRoute"))
app.use("/Qry", require("./routes/queryRoute"))
app.use("/Count", require("./routes/CountRoute"))
app.use("/SubCatList", require("./routes/SubCatListRoute"))
app.use("/contact", require("./routes/contactRoutes"))
app.use("/font", require("./routes/fontRoutes")) 


app.listen(3001, function () {
    console.log("local hosting Server is running on port 3001");
})
 
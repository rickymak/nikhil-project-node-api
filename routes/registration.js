
const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require("multer");
const upload = multer({ dest: 'uploads/' })
const Reg = mongoose.model("registration");
const Blog = mongoose.model('Blog')
const Main = mongoose.model("MainCateory");
var bodyParser = require('body-parser')
var customParser = bodyParser.json({
    type: function (req) {
        req.headers['content-type'] === star / star
    }
});


router.post("/registration", async (req, res) => {
    let body = { name: req.body.name, address: req.body.address }
    console.log("body registration", body)
    console.log("Data registration>>>", req.body);
    const queryCheck = await Reg.findOne({ $or: [{ name: req.body.name }, { phone: req.body.phone }] });

    try {
        if (!queryCheck) {
            const reg = new Reg();
            reg.name = req.body.name;
            reg.email = req.body.email;
            reg.password = req.body.password;
            reg.phone = req.body.phone;
            reg.state = req.body.state;
            reg.city = req.body.city;
            reg.address = req.body.address;
            reg.type = req.body.type;
            reg.gender = req.body.gender;
            reg.UserType = req.body.UserType;
            reg.age = req.body.age;
            reg.open = "Open";
            await reg.save();
            // res.send(reg)
            res.send({
                data: reg,
                ResponeCode: 'Successfully Saved',
                message: 'Registration Successfully',
                status: true
            })
        }
        else if (queryCheck) {
            res.send({
                message: 'Duplicate',
                status: false
            })
        }

    } catch (error) {
        res.status(500)
    }

});


router.post("/login", async (req, res) => {
    console.log("body>>>>>>>>>>>>>", req.body)

    try {
        let user = await Reg.findOne({ phone: req.body.phone });
        if (user) {

            const reg = await Reg.findOne({ phone: req.body.phone, password: req.body.password })

            if (reg != null) {
                res.json({
                    success: true,
                    ResponeCode: 'Successful',
                    message: 'Authentication successful!',
                    data: reg,
                    status: true,
                    check: "ricky1",

                });
            }
            else {
                res.json({
                    ResponeCode: 'Failed',
                    status: false,
                    message: 'Please Check Your Email or Password',

                });
            }

        }

        res.json({
            ResponeCode: 'User Not Found',
            status: false,
            message: 'User Phone Number Not Be Registration',

        });



    } catch (error) {
        res.status(500);
    }
});

router.get("/allvendor", async (req, res) => {
    console.log(req.name)
    try {
        const reg = await Reg.find({ $and: [{ UserType: "Partner" }, { open: "Open" }] })
        res.send({
            data: reg,
            ResponeCode: 'Successful',
            status: true
        })
    } catch (error) {
        res.status(500)
    }
});
router.get("/allUser", async (req, res) => {
    console.log(req.name)
    try {
        const reg = await Reg.find({UserType:'User'})
        res.send({
            data: reg,
            ResponeCode: 'Successfully',
            status: true
        })
    } catch (error) {
        res.status(500)
    }
});
router.get("/vendorlist", async (req, res) => {
    console.log(req.name)
    try {
        const reg = await Reg.find({ $and: [{ UserType: "Partner" }, { open: "Closed" }] })
        res.send({
            data: reg,
            ResponeCode: 'Successful',
            status: true
        })
    } catch (error) {
        res.status(500)
    }
});

router.post("/VAC", async (req, res) => {
    const getSub = await Blog.findOne({ _id: req.body.SelectCategory })
    const getMain = await Main.findOne({ _id: getSub.MaincategoryID })
    try {
        const reg = await Reg.update(
            { _id: req.body.VendorId },
            {
                $set: {
                    "Category": getMain.MainCateory,
                    "CategoryId": getSub._id,
                    "SubCategory": getSub.Subcategory,
                    "SubCategoryId": getSub._id,
                    "open": "Closed",

                }
            }
        )

        res.send({
            update: reg,
            ResponeCode: 'Successfully',
            status: true,

        })

    } catch (error) {
        res.send(500)
    }
})
router.post("/getPassword", async (req, res) => {
    console.log(req.name)
    try {
        // const get = await Reg.findOne({email: req.body.email})
        const get = await Reg.findOne({ $or: [{ email: req.body.email }, { phone: req.body.email }] });
        if (get != null) {
            console.log('get>>>>>>>>>>>>>>>>>>>', get)
            res.send({
                message: get.password,
                ResponeCode: 'Successful',
                status: true
            })
        }
        else {
            console.log('get>>>>>>>>>>>>>>>>>>>', get)
            res.send({
                message: 'Email address/Phone Number does Not Exit',
                status: true
            })
        }

    } catch (error) {
        res.status(500)
    }
});
router.post("/ChangePassword", async (req, res) => {
    try {
        const get = await Reg.findOne({ _id: req.body.postid })
        console.log('get>>>>>>>>',get)
        if (get != null) {
            if (get.password == req.body.password) {
                try {
                    const reg = await Reg.update(
                        { _id: req.body.postid },
                        {
                            $set: {
                                "password": req.body.NewPassword,
                            }
                        }
                    )
                    res.send({
                        update: reg,
                        status: true,
                        message: 'Password Updated',
                    })
                } catch (error) {
                    res.send(500)
                }
            }
            else {
                res.send({
                    status: false,
                    message: 'Password Does Not Match',
                })
            }


        }
        if(get == null) {
            console.log('get>>>>>>>>>>>>>>>>>>>', get)
            res.send({
                message: 'User Not Be Valid',
                status: false
            })
        }
    } catch (error) {
        res.status(500)
    }
});

router.post("/ProfileUpate", async (req, res) => {
  
    try {       
        const profile = await Reg.update(
            { _id: req.body.postId },
            { $set: {
                 "name" : req.body.fullname,
                 "phone" : req.body.contactnumber,
                 "state" : req.body.state,
                  "city" : req.body.city,
                 "address" : req.body.address,                  
                 "gender" : req.body.gender,                  
                 "age" : req.body.age,
                
                } }
         )

         res.send({
            data:profile,
            message: 'Profile Update Successfully',     
            status: true
        })
  
    } catch (error) {
        res.send(500)
    }
  })
  router.post("/VendorUpate", async (req, res) => {  
    try {       
        const profile = await Reg.update(
            { _id: req.body.postId },
            { $set: {                                  
                 "open" : 'Closed',
                } }
         )
         res.send({
            data:profile,
            message: 'Vendor Update Successfully',     
            status: true
        })
  
    } catch (error) {
        res.send(500)
    }
  })


  router.post("/VendorCategoryUpate", async (req, res) => {  
    const getSub = await Blog.findOne({ _id: req.body.SubCategoryId })    
    // const MainID = getSub.MaincategoryID;
    // const MainCat = getSub.MainCategory;
    // const subCatName = getSub.Subcategory; 
    // console.log('MainID>>>>>',MainID + 'MainCat' + MainCat +'subCatName' + subCatName)
    console.log('Get Details Sub Category>>>>>',getSub)
    console.log('Vendor ID>>>>>',req.body.postId)
    try {       
        const profile = await Reg.update(
            { _id: req.body.postId },
            { $set: { 
                "SubCategoryId": req.body.SubCategoryId,
                "SubCategory": getSub.Subcategory, 
                "Category":  getSub.MainCategory,
                "CategoryId":getSub.MaincategoryID,

                } }
         )
         res.send({
            data:profile,
            message: 'Vendor Category Successfully',     
            status: true
        })
  
    } catch (error) {
        res.send(500)
    }
  })

router.post("/vendorByCategoryList", async (req, res) => {
    console.log('req.body.SubCategory>>>', req.body.SubCategory)
    try {
        const reg = await Reg.find({ $and: [{ UserType: "Vendor" }, { SubCategory: req.body.SubCategory }] })
        res.send({
            data: reg,
            ResponeCode: 'Successful',
            status: true
        })
    } catch (error) {
        res.status(500)
    }
});

router.post("/getUserVendor", async (req, res) => {
    console.log(req.name)
    try {
        // const get = await Reg.findOne({email: req.body.email})
        const get = await Reg.findOne({_id: req.body.postId});
        if (get != null) {
            console.log('get>>>>>>>>>>>>>>>>>>>', get)
            res.send({
                data: get,
                ResponeCode: 'Successful',
                status: true
            })
        }
        else {
            console.log('get>>>>>>>>>>>>>>>>>>>', get)
            res.send({
                message: 'User Does Not Exit',
                status: false
            })
        }

    } catch (error) {
        res.status(500)
    }
});


module.exports = router;
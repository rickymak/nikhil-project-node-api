const router = require("express").Router();
const mongoose = require("mongoose");
const cloudinary = require('cloudinary')
// require('./handlers/cloudinary')
const Blog = mongoose.model('Blog')
require('../handlers/cloudinary')
const upload = require('../handlers/multer')
const Main = mongoose.model("MainCateory");
var bodyParser = require('body-parser')
//const jwt = require('jsonwebtoken');
//let config = require('../config');
var customParser = bodyParser.json({type: function(req) {
       req.headers['content-type'] === star/star
}});




router.get("/all", async (req, res) => {
    console.log(req.name)
    try {
        const main = await Main.find({})
        res.send({
            data:main,
            ResponeCode: 'Successful',     
            status: true
        })
    } catch (error) {
        res.status(500)
    }
});
router.post("/save", upload.single('image'), async (req, res) => {
    console.log( 'Main Category data>>>>>',req.body)
    const queryCheck = await Main.findOne({ MainCateory: req.body.MainCateory })    
  if(!queryCheck){
    console.log('ricky 1>>>>>>>>>>>',req.file.path)
      const result = await cloudinary.v2.uploader.upload(req.file.path)
      console.log('ricky>>>>>>>>>>>',result.secure_url)
      const main = new Main()
      main.MainCateory = req.body.MainCateory;   
      main.imageUrl = result.secure_url,    
      await main.save()
      res.send({
        message: 'Successful',
        status: true
      })
  }
  else if(queryCheck){
    res.send({
      message: 'Duplicate Sub category',
      status: false
    })
  }
  //app.post('/create_blog', upload.single('image'), async (req, res) => {
   
    })
router.post("/updateId", async (req, res) => {
    console.log('Update by Id Ricky>>>>>',req.body)
    try {
        const main = await Main.findByIdAndUpdate({
            _id: req.body.postId
        }, req.body, {
            new: true,
            runValidators: true
        });

        res.send(main)

    } catch (error) {
        res.send(500)
    }
});

router.post("/findById", async (req, res) => {
    console.log('id Pass >>>>>>',req.body)
    try {
        const blog = await Blog.find({ MaincategoryID: req.body.postId })
        res.send(blog)
    } catch (error) {
        res.status(500);
    }
  });


  router.post("/deleteId", async (req, res) => {
    // console.log("Main Category id >>>>>>>.",req.body.postId)
     const blog = await Blog.findOne({ MaincategoryID: req.body.postId })
      console.log('Id>>>>>>',blog)
     Main.findByIdAndRemove({_id: req.body.postId}, function(err, result) {
        if (err) {
            console.log(err);
        }
        else if(result){
            console.log('blog._id>>>>>>>',blog)
            if(blog != null){
                Blog.deleteMany({MaincategoryID: blog.MaincategoryID}, function(err, result) {
                    if (err) {
                        console.log(err);
                    }
                    else if(result){
                        res.send({
                                  ResponeCode: 'Main and Sub Are Deleted Successful',     
                                  status: true
                              })
                    }
                });
            }
           else if(blog == null){
            res.send({
                ResponeCode: 'Main Deleted Successful But Sub Not Be Deleted',     
                status: true
            })
           }
        }
    });
 
  });

  router.post("/SubCatUpdate", upload.single('image'), async (req, res) => {
    console.log("SubCatUpdate body   >>>>>>>.",req.body) 
    const result = await cloudinary.v2.uploader.upload(req.file.path)   
    try {       
        const main = await Main.update(
            { _id: req.body.postId },
            { $set: {
                "MainCateory" : req.body.MainCateory,   
                "imageUrl" : result.secure_url, 

                
                } }
         )
  
         res.send({
            data:main,
            ResponeCode: 'Upadate',     
            status: true
        })
  
    } catch (error) {
        res.send(500)
    }
  });
    router.post("/UpdatewithOutImage", upload.single('image'), async (req, res) => {
      console.log("UpdatewithOutImage id >>>>>>>.",req.body.postId)
      try {       
          const main = await Main.update(
              { _id: req.body.postId },
              { $set: {
                "MainCateory" : req.body.MainCateory, 
                  } }
           )
  
           res.send({
              data:main,
              ResponeCode: 'ricky update with our image',     
              status: true
          })
    
      } catch (error) {
          res.send(500)
      }
    });
   


module.exports = router;
const router = require("express").Router();
const mongoose = require("mongoose");
const cloudinary = require('cloudinary')
// require('./handlers/cloudinary')
require('../handlers/cloudinary')
const upload = require('../handlers/multer')
const Blog = mongoose.model('Blog')
const Main = mongoose.model("MainCateory");
const Price = mongoose.model('Pricelist')
const Image = mongoose.model("UploadImg");
 var bodyParser = require('body-parser')
  //// Image Upload Code 

  var express = require('express');
// var router = express.Router();
var multer = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + '.jpg')
  }
});
var upload1 = multer({ storage: storage }).single('profileImage');
router.post('/Image', function (req, res) {
  upload1(req, res, function (err) {
    res.json({
      success: true,
      message: 'Image uploaded!'
  });
    console.log('upload >>>>>', res)
      if (err) {
          // An error occurred when uploading
      }
      // Everything went fine
  })
});

// router.post('/Image1', upload.single('profileImage'), (req, res, next) => {
//   // Create a new image model and fill the properties
//   let newImage = new Image();
//   newImage.filename = req.file.filename;
//   newImage.originalName = req.file.originalname;
//   newImage.desc = req.body.desc
//   newImage.save(err => {
//       if (err) {
//           return res.sendStatus(400);
//       }
//       res.status(201).send({ newImage });  
//   });
// });


module.exports.uploadImage = function (req, res) {
  uploadImageHelper.uploadImage(req, res, function (err, result) {
      if (err) {
          return res.status(500).json({message: 'Internal server error.'});
      }
      return res.status(200).json({'message': 'Image is uploaded', 'image': result.image});
  });
};
  /// image upload cancel    
 
var customParser = bodyParser.json({type: function(req) {
       req.headers['content-type'] === star/star
}});
router.post("/create_blog", upload.single('image'), async (req, res) => {
  console.log('Save Main Category>>>>',req.body)
  const queryCheck = await Blog.findOne({ MaincategoryID: req.body.MaincategoryID , Subcategory: req.body.Subcategory }) 
  const getMainCategory = await Main.findOne({ MainCateory: req.body.MaincategoryID}) 
  console.log('Get Main Category Name>>>>>>',getMainCategory)
if(!queryCheck){  
    const result = await cloudinary.v2.uploader.upload(req.file.path)   
    const blog = new Blog()
    blog.MaincategoryID = getMainCategory._id,
    blog.imageUrl = result.secure_url,
    blog.Subcategory = req.body.Subcategory,
    blog.MainCategory = getMainCategory.MainCateory,
    await blog.save()
    res.send({
      message: 'Successful',
      status: true
    })
}
else if(queryCheck){
  res.send({
    message: 'Duplicate Sub category',
    status: false
  })
}
//app.post('/create_blog', upload.single('image'), async (req, res) => {
 
  })

  router.post("/saveImg", upload.single('image'), async (req, res) => {
    console.log('ricky 1>>>>>>>>>>>', res.body)
      // const result = await cloudinary.v2.uploader.upload(req.file.path)
      // console.log('ricky>>>>>>>>>>>',result.secure_url)
     
      res.send({
        message: 'Successful',
        status: true,
        url:'asd',
      })
    })



  router.post("/deleteId", async (req, res) => {
    // console.log("Main Category id >>>>>>>.",req.body.postId)
     const PriceDetals = await Price.findOne({ SubCategoryID: req.body.postId })
      console.log('Id>>>>>>',PriceDetals)
      Blog.findByIdAndRemove({_id: req.body.postId}, function(err, result) {
        if (err) {
            console.log(err);
        }
        else if(result){
            console.log('PriceDetals._id>>>>>>>',PriceDetals)
            if(PriceDetals != null){
              Price.deleteMany({SubCategoryID: PriceDetals.SubCategoryID}, function(err, result) {
                    if (err) {
                        console.log(err);
                    }
                    else if(result){
                        res.send({
                                  ResponeCode: 'Sub and Price Are Deleted Successful',     
                                  status: true
                              })
                    }
                });
            }
           else if(PriceDetals == null){
            res.send({
                ResponeCode: 'Sub Deleted Successful But Price Not Be Deleted',     
                status: true
            })
           }
        }
    });

  });


  router.get("/all", async (req, res) => {
    console.log(req.name)
    try {
        const main = await Blog.find({})
        res.send({
            data:main,
            ResponeCode: 'Successful',     
            status: true
        })
    } catch (error) {
        res.status(500)
    }
});

router.post("/findById", async (req, res) => {
  console.log('id Pass >>>>>>',req.body)
  try {
      const blog = await Blog.find({ MaincategoryID: req.body.postId })

      if(blog.length > 0){
        res.send({
          data:blog,
          ResponeCode: 'Successful',     
          status: true
      })
      }
     else if(blog.length == 0){
        res.send({
          status: false
      })
      }
  } catch (error) {
      res.status(500);
  }
});

  router.post("/SubCatUpdate", upload.single('image'), async (req, res) => {
  console.log("SubCatUpdate body   >>>>>>>.",req.body) 
  const getMainCategory = await Main.findOne({ MainCateory: req.body.MaincategoryID}) 
  const result = await cloudinary.v2.uploader.upload(req.file.path)   
  try {       
      const blog = await Blog.update(
          { _id: req.body.postId },
          { $set: {
             
               "MaincategoryID":getMainCategory._id,
               "imageUrl" : result.secure_url,
              "Subcategory" : req.body.Subcategory,
              "MainCategory" :  getMainCategory.MainCateory,
              
              } }
       )

       res.send({
          data:blog,
          ResponeCode: 'Upadate',     
          status: true
      })

  } catch (error) {
      res.send(500)
  }
});





  router.post("/UpdatewithOutImage", upload.single('image'), async (req, res) => {
    console.log("UpdatewithOutImage id >>>>>>>.",req.body)
    const getMainCategory = await Main.findOne({ MainCateory: req.body.MaincategoryID}) 
    console.log('getMainCategory>>>>>>>',getMainCategory)
    try {       
        const blog = await Blog.update(
            { _id: req.body.postId },
            { $set: {
              "MaincategoryID":getMainCategory._id,               
              "Subcategory" : req.body.Subcategory,
              "MainCategory" : getMainCategory.MainCateory,
                
                } }
         )

         res.send({
            data:blog,
            ResponeCode: 'ricky update with our image',     
            status: true
        })
  
    } catch (error) {
        res.send(500)
    }
  })


module.exports = router;